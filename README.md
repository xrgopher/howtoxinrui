# Introduction.

This is the simple guideline for how to play Chinese version of xinrui/synrey app, which is not official.

The english version can be checked [Play online with Synrey Bridge](http://greatbridgelinks.com/play-online-with-synrey-bridge/)

Compare to bbo/funbridge, xinrui provides different AI system with different features

## Registration

It is not openned to external, you need a Chinese mobile phone number, so better ask friends there to register for you.

> maybe it is possible to login via wechat (not tried), wechat is kind of Chinese facebook

The payment can be done later via Apply play or your friends (if Android)

## Bidding system

* CCBA 2.2 based on 2 over 1 (bidding card will be available soon)

## Guideline

see [guideline.md](guideline.md)

## Match recommendation

Most valuable

* monthly individual competition to reach different level (start from 1st to 9th)
* monthly group competition in pair ( start from 19th)
* monthly group competition in 4 people (start from 10th)
* Grand Prix every month (start from 19th)

Others feel free to try

After registered, it will popup in "my match"

## Credits

It is called xinrui coins inside, some match needs conins to pay and also gain points if you win

## Support

* search for us

# Reference

* http://xinruibridge.com/
 