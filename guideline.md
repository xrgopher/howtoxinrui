# Guideline

very simple guideline

## Login

![](image/login.png)

## Main page

![](image/main.png)

Need to click the button to register some matchs in advance, after registered, it will popup in "my match"

## Play the game

it is tricky now to understand the hints now.. use CCBA bidding card for help

## After the game

![](image/afterboard.png)

## More for the game

![](image/more.png)

Click "more" to open another menu which is used to share and bookmark

* share to others in chat is really nice feature, so others can give comment for your game
* share to wechat is used to collect xinrui games via its link like [sample game](http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P,1C,3D%3BX,P,4D,P%3B4H,P,P,P%3B&playlog=W:6D,QD,KD,AD%3BS:TC,JC,QC,7C%3BN:AH,3H,2H,5H%3BN:QH,TH,6H,9H%3BN:4H,5S,KH,JH%3BS:9C,KC,AC,2D%3BN:8C,3D,3C,2C%3BN:6C,8D,5C,4C%3BN:TS,8S,2S,QS%3BW:9D,3S,7D,8H%3BS:4S,9S,KS,JD%3BN:&deal=AQ9.J95.964.KJ42%20KT73.AQ74.Q.AQ86%2085.T3.KJT87532.7%20J642.K862.A.T953&vul=None&dealer=W&contract=4H&declarer=S&wintrick=11&score=450&str=%E7%BE%A4%E7%BB%84IMP%E8%B5%9B%2020210216%20%E7%89%8C%E5%8F%B7%208/8&dealid=1036100987&pbnid=351519488), it contains all the information for replay, it can be converted to pbn as well, see [bridge-utils](https://gitlab.com/xrgopher/bridge-utils)

## How to register the match

Monthly competition is one of the best place to compete with each other, it runs every month for 9 rounds (shorten from 12 rounds), start with basic level, and grows to next level if you are in top list.

![](image/register.png)

When the match is registered, you can wait for the time to attend.

Be aware of the timezone, most of the game is started at beijing time (GMT+8) 0:10 and ends in 23:59 




